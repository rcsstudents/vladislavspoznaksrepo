﻿using System;

namespace Models
{
    public class Student
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int UniversityId { get; set; }

        public string FullName()
        {
            return Name + " " + Surname;
        }
    }
}
