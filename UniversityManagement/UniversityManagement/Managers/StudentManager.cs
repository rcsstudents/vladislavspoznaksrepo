﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataManager.ManageData;
using Models;

namespace UniversityManagement.Managers
{
    public class StudentManager : IManager
    {
        public List<Student> Students;
        DataReader reader;
        DataWriter writer;

        public StudentManager()
        {
            reader = new DataReader(DataConfiguration.StudentData);
            writer = new DataWriter(DataConfiguration.StudentData);

            Students = reader.ReadData<Student>();
         }

        public void Create()
        {
            UniversityManager universityManager = new UniversityManager();
            universityManager.Read();
            //University studentUniversity = new University();

            Console.WriteLine("Expected input - Name Surname StudentID UniversityID");
            Console.Write("Enter student information: ");
            string dataInput = Console.ReadLine();
            List<string> dataList = dataInput.Split(' ').ToList();

            Student student = new Student();
            int universityId = Convert.ToInt32(dataList[3]);

            University studentUniversity = universityManager.Universities.FirstOrDefault(u => u.Id == universityId);

            if (studentUniversity == null)
            {
                Console.WriteLine($"University with ID - {universityId} does not exsit.");
                return;
            }

            student.Name = dataList[0];
            student.Surname = dataList[1];
            student.Id = Convert.ToInt32(dataList[2]);
            student.UniversityId = universityId;

            Students.Add(student);
            writer.WriteData(Students);

            Console.WriteLine($"Student {student.FullName()} was created and added successfully!");
        }

        public void Delete()
        {
            Console.WriteLine("Expected input - StudentID");
            Console.Write("Enter student ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            Students = Students.Where(s => s.Id != id).ToList();
            writer.WriteData(Students);
            Console.WriteLine($"Student with {id} was found and deleted successfully!");

        }

        public void Read()
        {
            UniversityManager universityManager = new UniversityManager();
            //universityManager.Read();
            University studentUniversity = new University();

            Console.WriteLine("List of Students.");
            Console.WriteLine();

            foreach (Student student in Students)
            {
                studentUniversity = universityManager.Universities.FirstOrDefault(u => u.Id == student.UniversityId);
                Console.WriteLine($"Student {student.FullName()}");
                Console.WriteLine($"ID number: {student.Id}");
                Console.WriteLine($"Studies in: {studentUniversity.Name}, ID - {studentUniversity.Id}.");
                Console.WriteLine();
            }

            Console.WriteLine("End of list!");
        }

        public void Update()
        {
            Console.WriteLine("Expected input - StudentID");
            Console.Write("Enter Student ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            Student student = Students.FirstOrDefault(s => s.Id == id);

            if (student == null)
            {
                Console.WriteLine($"Student with ID - {id} does not exsit.");
                return;
            }

            UniversityManager universityManager = new UniversityManager();
            universityManager.Read();
            //University studentUniversity = new University();

            Console.WriteLine("Expected input - Name Surname StudentID UniversityID");
            Console.Write("Enter student information: ");
            string dataInput = Console.ReadLine();
            List<string> dataList = dataInput.Split(' ').ToList();

            //Student student = new Student();
            int universityId = Convert.ToInt32(dataList[3]);

            University studentUniversity = universityManager.Universities.FirstOrDefault(u => u.Id == universityId);

            if (studentUniversity == null)
            {
                Console.WriteLine($"University with ID - {universityId} does not exsit.");
                return;
            }

            student.Name = dataList[0];
            student.Surname = dataList[1];
            student.Id = Convert.ToInt32(dataList[2]);
            student.UniversityId = universityId;

        }
    }
}
