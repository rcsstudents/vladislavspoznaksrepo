﻿using System;
namespace UniversityManagement.Managers
{
    public interface IManager
    {
        void Create();
        void Read();
        void Update();
        void Delete();
    }
}
