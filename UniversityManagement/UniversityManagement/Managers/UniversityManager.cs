﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataManager.ManageData;
using Models;

namespace UniversityManagement.Managers
{
    public class UniversityManager : IManager
    {
        public List<University> Universities;
        DataReader reader;
        DataWriter writer;

        public UniversityManager()
        {
            reader = new DataReader(DataConfiguration.UniversityData);
            writer = new DataWriter(DataConfiguration.UniversityData);

            Universities = reader.ReadData<University>();
        }
        public void Create()
        {
            Console.WriteLine("Expected input - UniversityID Name");
            Console.WriteLine("Enter university information: ");
            string dataInput = Console.ReadLine();
            List<string> dataList = dataInput.Split(' ').ToList();

            University university = new University();
            university.Id = Convert.ToInt32(dataList[0]);
            university.Name = dataList[1];

            Universities.Add(university);
            writer.WriteData(Universities);

            Console.WriteLine("University created and added successfully!");
        }

        public void Delete()
        {
            Console.WriteLine("Expected input - UniversityID");
            Console.Write("Enter university ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            University university = Universities.FirstOrDefault(u => u.Id == id);
            if (university == null)
            {
                Console.WriteLine($"University with ID - {id} does not exsit.");
                return;
            }

            Universities = Universities.Where(u => u.Id != id).ToList();
            writer.WriteData(Universities);
            Console.WriteLine($"University with {id} was found and deleted successfully!");
        }

        public void Read()
        {
            Console.WriteLine("List of Universities.");
            Console.WriteLine();

            foreach (University university in Universities)
            {
                Console.WriteLine($"ID number: {university.Id}");
                Console.WriteLine($"University {university.Name}");
                //Console.WriteLine();
            }

            Console.WriteLine("End of list!");
        }

        public void Update()
        {
            Console.WriteLine("Expected input - UniversityID");
            Console.Write("Enter university ID: ");
            int id = Convert.ToInt32(Console.ReadLine());

            University university = Universities.FirstOrDefault(u => u.Id == id);

            if (university == null)
            {
                Console.WriteLine($"University with ID - {id} does not exsit.");
                return;
            }

            Console.WriteLine($"University with ID - {university.Id}  name is {university.Name}");
            Console.Write("Enter university name: ");
            string name = Console.ReadLine();

            university.Name = name;
            writer.WriteData(Universities);
        }
    }
}
