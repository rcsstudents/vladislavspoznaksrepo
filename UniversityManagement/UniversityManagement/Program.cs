﻿using System;
using UniversityManagement.Managers;

namespace UniversityManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            bool end = false;
            while (!end)
            {
                Console.WriteLine("Welcome to Univesity Management, GENERAL!");
                Console.WriteLine();
                Console.WriteLine("0 - Use Student");
                Console.WriteLine("1 - Use University");
                Console.Write("Enter option: ");
                string entityTypeString = Console.ReadLine();
                int entityType = Convert.ToInt32(entityTypeString);

                IManager manager = GetEntityManager((EntityType)entityType);
                DoDataActions(manager);

                Console.Write("Stop (Y/N): ");
                if (Console.ReadLine().Equals("Y", StringComparison.InvariantCultureIgnoreCase))
                {
                    end = true;
                }
            }

        }

        private static IManager GetEntityManager(EntityType entityType)
        {
            IManager manager = null;

            switch(entityType)
            {
                case EntityType.Student:
                    manager = new StudentManager();
                    break;
                case EntityType.University:
                    manager = new UniversityManager();
                    break;
                default:
                    Console.WriteLine($"Cannot find entity type with provided ID - {(int)entityType}");
                    break;
            }

            return manager;
        }

        private static void DoDataActions(IManager manager)
        {
            Console.WriteLine();
            Console.WriteLine("0 - Create");
            Console.WriteLine("1 - Read");
            Console.WriteLine("2 - Update");
            Console.WriteLine("3 - Delete");
            Console.Write("Enter option: ");
            string dataActionString = Console.ReadLine();
            int dataAction = Convert.ToInt32(dataActionString);

            switch ((DataAction)dataAction)
            {
                case DataAction.Create:
                    manager.Create();
                    break;
                case DataAction.Read:
                    manager.Read();
                    break;
                case DataAction.Update:
                    manager.Update();
                    break;
                case DataAction.Delete:
                    manager.Delete();
                    break;
                default:
                    Console.WriteLine("Unexpected input!");
                    break;
            }
        }
    }
}
