﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models.EmployeeModels;

namespace EmployeeGenerator
{
    public class GenerateEmployees
    {
        private List<string> GenerateData(string dataType)
        {
            Console.Write($"Write {dataType}: ");
            string data = Console.ReadLine();
            List<string> dataList = data.Split(' ').ToList();

            return dataList;
        }

        public Vacation GenerateVacation()
        {
            Console.WriteLine("Expected input: Reason 'Start'gggg mm dd 'End'gggg mm dd");
            List<string> vacationData = GenerateData("vacation");
            Vacation vacation = new Vacation();

            vacation.Reason = vacationData[0];
            vacation.StartDate = new DateTime(
                Convert.ToInt32(vacationData[1]),
                Convert.ToInt32(vacationData[2]),
                Convert.ToInt32(vacationData[3]));

            vacation.EndDate = new DateTime(
                Convert.ToInt32(vacationData[4]),
                Convert.ToInt32(vacationData[5]),
                Convert.ToInt32(vacationData[6]));

            return vacation;
        }

        public Employee GenerateEmployee()
        {
            Console.WriteLine("Expected input: Name Surname Age ");
            List<string> employeeData = GenerateData("employee");
            Employee employee = new Employee();

            employee.Name = employeeData[0];
            employee.Surname = employeeData[1];
            employee.Age = Convert.ToInt32(employeeData[2]);

            //string employeeTypeString = employeeData[3];
            //int vertiba = (int)EmployeeType.Manager;

            //EmployeeType typeVertiba = EmployeeType.Manager;
            //employee.EmployeeType = (EmployeeType)employeeTypeString;
            //int emplotint = Convert.ToInt32(employeeData[3]);
            //employee.EmployeeType = (EmployeeType)emplotint;

            employee.Vacation = GenerateVacation();

            return employee;
        }

    }
}
