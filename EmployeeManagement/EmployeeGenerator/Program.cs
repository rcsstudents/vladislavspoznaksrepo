﻿using System;
using DataManager.ManageData;
using Models.EmployeeModels;

namespace EmployeeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome, General to Employee Generator!");

            bool generate = true;
            while (generate)
            {
                try
                {
                    GenerateEmployees generateEmployee = new GenerateEmployees();
                    Employee employee = generateEmployee.GenerateEmployee();

                    DataWriter writer = new DataWriter(DataConfiguration.EmployeeData);
                    writer.WriteEmployee(employee);
                    Console.WriteLine($"Employee {employee.FullName()} added successfully!");

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.Write("Generate more employees (Y/N): ");
                string answer = Console.ReadLine();

                generate = answer.Equals("Y", StringComparison.InvariantCultureIgnoreCase);

            }
        }
    }
}
