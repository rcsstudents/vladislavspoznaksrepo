﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace DataManager.ManageData
{
    public class DataReader
    {
        public readonly string FileName;

        public DataReader(string fileName)
        {
            FileName = fileName;
        }

        public List<T> ReadData<T>()
        {
            using (StreamReader reader = new StreamReader(FileName))
            {
                string dataFromFile = reader.ReadToEnd();
                List<T> data = JsonConvert.DeserializeObject<List<T>>(dataFromFile);

                Console.WriteLine("Data list was read successfully!");
                return data ?? new List<T>();
            }
        }
    }
}
