﻿using System;
namespace DataManager.ManageData
{
    public static class DataConfiguration
    {
        public static readonly string EmployeeData =
            "/Users/vladislavs.poznaks/GIT Repository/vladislavspoznaksrepo/EmployeeManagement/EmployeeData.txt";

        public static readonly string StudentData =
           "/Users/vladislavs.poznaks/GIT Repository/vladislavspoznaksrepo/UniversityManagement/StudentData.txt";

        public static readonly string UniversityData =
           "/Users/vladislavs.poznaks/GIT Repository/vladislavspoznaksrepo/UniversityManagement/UniversityData.txt";
    }
}
