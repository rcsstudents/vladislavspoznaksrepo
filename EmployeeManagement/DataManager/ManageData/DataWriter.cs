﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
namespace DataManager.ManageData

{
    public class DataWriter
    {
        public readonly string FileName;

        public DataWriter(string fileName)
        {
            FileName = fileName;
        }

        public void WriteData<T>(List<T> data)
        {
            using (StreamWriter writer = new StreamWriter(FileName))
            {
                string dataJson = JsonConvert.SerializeObject(data);
                writer.WriteLine(dataJson);
                Console.WriteLine("Data was written successfully!");
            }
        }
    }
}
