﻿using System;

namespace EmployeeManagement
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("GENERAL, Welcome to Employee ('mercenary') Management system!");

            //Console.WriteLine("Employee: ");
            int option = 1;
            EmployeeManager manager = new EmployeeManager();
            manager.ManageEmployees();

            int chosenOption = 1;
            //MainMenuEnum mainMenu;

            while (chosenOption != 0)
            {
                manager.MainMenu();
                chosenOption = Convert.ToInt32(Console.ReadLine());

                switch (chosenOption)
                {
                    case (int)MainMenuEnum.ListEmployees:
                        try
                        {
                            manager.ListEmployees();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                        break;
                    case (int)MainMenuEnum.RemoveEmployee:
                        manager.RemoveEmployee();
                        break;
                    case (int)MainMenuEnum.ChangeEmployeeAge:
                        manager.ChangeAge();
                        break;
                    case (int)MainMenuEnum.EndWork:
                        break;
                    default:
                        Console.WriteLine("Unrecognized command... Try again!");
                        break;
                }
            }
        }
    }
}
