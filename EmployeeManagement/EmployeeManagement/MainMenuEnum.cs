﻿using System;
namespace EmployeeManagement
{
    public enum MainMenuEnum
    {
        ListEmployees = 1,
        RemoveEmployee = 2,
        ChangeEmployeeAge = 3,
        EndWork = 0
    }
}
