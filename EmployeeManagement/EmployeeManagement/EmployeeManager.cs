﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataManager.ManageData;
using Models.EmployeeModels;

namespace EmployeeManagement
{
    public class EmployeeManager
    {
        public List<Employee> EmployeeList = new List<Employee>();

        public void ManageEmployees()
        {
            DataReader reader = new DataReader(DataConfiguration.EmployeeData);
            EmployeeList.AddRange(reader.ReadEmployees());
            Console.WriteLine("Employee list created successfully!");
            //Employee employee = reader.ReadEmployee();
        }

        public void ListEmployees()
        {
            foreach (Employee employee in EmployeeList)
            {
                Console.WriteLine($"Full name: {employee.FullName()}");
                Console.WriteLine($"Age: {employee.Age}");
                Console.WriteLine($"Vacation reason: {employee.Vacation.Reason}");
                Console.WriteLine($"Vacation Start Date: {employee.Vacation.StartDate}");
                Console.WriteLine($"Vacation End Date: {employee.Vacation.EndDate}");

            }
        }

        public void RemoveEmployee()
        {
            Console.Write("Enter employee's Name and Surname: ");
            string employeeString = Console.ReadLine();

            List<string> dataList = employeeString.Split(' ').ToList();
            string name = dataList[0];
            string surname = dataList[1];

            foreach (Employee employee in EmployeeList)
            {
                if (employee.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase) 
                    && employee.Surname.Equals(surname, StringComparison.InvariantCultureIgnoreCase))
                {
                    EmployeeList.Remove(employee);
                    Console.WriteLine($"Employee {employee.FullName()} found and removed!");
                    break;
                }
            }
        }

        public void ChangeAge()
        {
            Console.Write("Enter employee's Name and Surname: ");
            string employeeString = Console.ReadLine();

            List<string> dataList = employeeString.Split(' ').ToList();
            string name = dataList[0];
            string surname = dataList[1];

            foreach (Employee employee in EmployeeList)
            {
                if (employee.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)
                    && employee.Surname.Equals(surname, StringComparison.InvariantCultureIgnoreCase))
                {
                    EmployeeList.Remove(employee);
                    Console.WriteLine($"Employee {employee.FullName()} found!");
                    Console.WriteLine($"Old age - {employee.Age}. Enter new age: ");
                    employee.Age = Convert.ToInt32(Console.ReadLine());
                }
            }
        }

        public void MainMenu()
        {
            Console.WriteLine("1 - List All Employees");
            Console.WriteLine("2 - Remove Employee by Name and Surname");
            Console.WriteLine("3 - Change Employee's age");
            Console.WriteLine("0 - End work");
            Console.Write("Choose an option: ");
        }
    }
}
