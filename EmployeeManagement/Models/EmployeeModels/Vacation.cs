﻿using System;
namespace Models.EmployeeModels
{
    [Serializable]
    public class Vacation
    {
        public string Reason { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
