﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace RCS_Day5_Lesson1
{
    class Program
    {
        static void Main(string[] args)
        {

            using (StreamWriter writer = File.AppendText("teksts.txt"))
            {
                writer.WriteLine("Hello!");
                //File.WriteAllText("teksts.txt", String.Empty);
            }

            List<string> lines = new List<string>();

            using (StreamReader reader = new StreamReader("teksts.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lines.Add(line);
                }

            }

            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }


            QuizQuestion quizQuestion = new QuizQuestion();
            quizQuestion.Question = "Jautājums";
            quizQuestion.Answer = "Atbilde";

            string json = JsonConvert.SerializeObject(quizQuestion);

            using (StreamWriter writer = new StreamWriter("MyJson.txt"))
            {
                writer.WriteLine(json);
            }

            string jsonFromFile;
            using (StreamReader reader = new StreamReader("MyJson.txt"))
            {
                jsonFromFile = reader.ReadToEnd();
            }

            QuizQuestion deserialized = JsonConvert.DeserializeObject<QuizQuestion>(jsonFromFile);

            Console.WriteLine(deserialized.Question);
            Console.WriteLine(deserialized.Answer);

        }
    }
}
