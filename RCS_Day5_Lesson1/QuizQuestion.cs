﻿using System;
namespace RCS_Day5_Lesson1
{
    [Serializable]
    public class QuizQuestion
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
